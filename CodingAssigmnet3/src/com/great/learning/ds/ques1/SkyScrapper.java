package com.great.learning.ds.ques1;

import java.util.Scanner;
import java.util.Stack;

public class SkyScrapper {

    private static void constructionProcess(int noOfFloors, int[] arraySize){
        Stack<Integer> stSize = new Stack<>();
        int highestSizeDay = noOfFloors;
        System.out.println("The order of construction is as follows");
        for(int i=0; i<noOfFloors; i++){
            int daySize = arraySize[i];
            if(daySize < highestSizeDay){
                stSize.push(daySize);
                System.out.println("Day: " + (i+1) + " ");
            }
            else {
                System.out.println("Day: " + (i+1));
                System.out.println(daySize + " ");
                highestSizeDay--;
                while(!stSize.isEmpty() && stSize.peek() >= highestSizeDay){
                    System.out.println(stSize.pop() + " ");
                    highestSizeDay--;
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of floors in the building: ");
        int n = scanner.nextInt();
        int[] arrSize = new int[n];
        for(int i=0; i<n ; i++){
            System.out.println("Enter the floor size given on Day: " + (i+1));
            arrSize[i] = scanner.nextInt();
        }
        constructionProcess(n, arrSize);
    }
}
