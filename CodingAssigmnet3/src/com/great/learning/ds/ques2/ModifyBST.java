package com.great.learning.ds.ques2;


public class ModifyBST {

    private Node currNode = null;
    private Node prevNode = null;

    static class Node{
        Node left;
        Node right;
        int data;

        public Node(int i) {
            this.data = i;
            this.left = null;
            this.right = null;
        }
    }

   public void modifyBST(Node root){
        if(root != null){
            modifyBST(root.left);
            Node right = root.right;

            if(currNode == null){
                currNode = root;
            } else {
                prevNode.right = root;
            }
            root.left = null;
            prevNode = root;

            modifyBST(right);
        }
   }

   public void displayTree(Node root){
        if(root != null){
            System.out.println(root.data);
            displayTree(root.right);
        }
   }

   public Node getModifiedBSTNode(){
        return currNode;
   }

   public static void main(String[] args){
        ModifyBST modifyBST = new ModifyBST();

        Node node = new Node(50);
        node.left = new Node(30);
        node.right = new Node(60);
        node.left.left = new Node(10);
        node.right.left = new Node(55);

        modifyBST.modifyBST(node);
        modifyBST.displayTree(modifyBST.getModifiedBSTNode());

   }

}
